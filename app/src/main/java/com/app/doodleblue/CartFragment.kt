package com.app.doodleblue

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.app.doodleblue.databinding.FragmetnCartBinding

class CartFragment : Fragment() {
    private lateinit var binding: FragmetnCartBinding
    private val viewModel: MainActivityViewModel by activityViewModels()

    private val adapter by lazy {
        DishAdapter(this, object : AdapterClickListener {
            override fun onItemClick(position: Int, value: CLICK) {
                when (value) {
                    CLICK.DELETE -> {
                        viewModel.deleteCartItem(position)
                    }
                    CLICK.ADD -> {
                        viewModel.addCartItem(position)
                    }
                }
            }

        })
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmetnCartBinding.inflate(
                inflater,
                container,
                false
        )
        binding.lifecycleOwner = this
        binding.layToolbar.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.rvOrders.adapter = adapter

        binding.tvShowMore.setOnClickListener {
            if (binding.tvShowMore.text == getString(R.string.show_more)) {
                binding.tvShowMore.text = getString(R.string.show_less)
            } else {
                binding.tvShowMore.text = getString(R.string.show_more)
            }
        }
        binding.layToolbar.tvTitle.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.cartList.observe(viewLifecycleOwner, {
            adapter.updateList(it ?: emptyList())
        })
    }
}
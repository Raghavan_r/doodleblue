package com.app.doodleblue

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.app.doodleblue.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: MainActivityViewModel by activityViewModels()

    private val adapter by lazy {
        DishAdapter(this, object : AdapterClickListener {
            override fun onItemClick(position: Int, value: CLICK) {
                when (value) {
                    CLICK.DELETE -> {
                        viewModel.deleteItem(position)
                    }
                    CLICK.ADD -> {
                        viewModel.addItem(position)
                    }
                }
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.rvDish.adapter = adapter

        binding.tvCart.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_cartFragment)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dishList.observe(viewLifecycleOwner,{
            adapter.updateList(it ?: emptyList())
        })

    }


}
package com.app.doodleblue

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.app.doodleblue.databinding.DishItemBinding
import com.app.doodleblue.generated.callback.OnClickListener

class DishViewHolder(private val itemBinding: DishItemBinding,
                     listener: AdapterClickListener) : RecyclerView.ViewHolder(itemBinding.root){

    val model= DishItemBinder(listener)

    fun bind(list: DishModel, postition: Int) {
        model.bind(list, postition)
        itemBinding.binder=model
        itemBinding.layQty.binder = model
        itemBinding.executePendingBindings()

    }

}
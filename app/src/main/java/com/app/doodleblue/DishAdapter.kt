package com.app.doodleblue


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.app.doodleblue.databinding.DishItemBinding


class DishAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val listener: AdapterClickListener
) :
    RecyclerView.Adapter<DishViewHolder>() {

    private var list = emptyList<DishModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DishViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = DishItemBinding.inflate(layoutInflater, parent, false)
        itemBinding.lifecycleOwner = lifecycleOwner
        itemBinding.layQty.lifecycleOwner = lifecycleOwner
        return DishViewHolder(
            itemBinding, listener
        )
    }

    override fun onBindViewHolder(holder: DishViewHolder, position: Int) {
        holder.bind(list[position],position)
    }

    override fun getItemCount() = list.size

    fun updateList(listItem: List<DishModel>) {
        this.list = listItem
        notifyDataSetChanged()
    }


}
package com.app.doodleblue

data class DishModel(
        val id: Int,
        val title: String,
        val description: String,
        var qty: Int,
        val costPerItem: Int
)
package com.app.doodleblue

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel :ViewModel(){


    private val _dishList = MutableLiveData<List<DishModel>>().apply {
        val data = emptyList<DishModel>().toMutableList()
        data.add(DishModel(1,"Snack","fruit, nuts, cherry, creams, cakes",3,23))
        data.add(DishModel(2,"veggies","bread, nuts, mils, creams, cakes",2,20))
        data.add(DishModel(3,"non veg","chicken, fish, mutton, meat, cakes",5,12))
        data.add(DishModel(4,"starter","lolipop, juice, soup, creams, cakes",7,11))
        data.add(DishModel(5,"Hard","fruit, nuts, cherry, creams, cakes",3,29))
        data.add(DishModel(6,"Softies","bread, nuts, mils, creams, cakes",2,22))
        data.add(DishModel(7,"Pizza","chicken, fish, mutton, meat, cakes",5,76))
        data.add(DishModel(8,"Breads","lolipop, juice, soup, creams, cakes",7,54))
        this.value = data
    }
    val dishList : LiveData<List<DishModel>> = _dishList

    private val _cartList = MediatorLiveData<List<DishModel>>().apply {
        addSource(_dishList){
            this.value = updateCartList()
        }
    }

    private fun updateCartList(): List<DishModel>? {
        val list = emptyList<DishModel>().toMutableList()
        _dishList.value?.forEach {
            if (it.qty !=0){
                list.add(it)
            }
        }
        return list
    }

    val cartList : LiveData<List<DishModel>> = _cartList

    val isShowMoreVisible = MediatorLiveData<Boolean>().apply{
        addSource(_cartList){
            value = it.size>2
        }
    }

    val qty = MediatorLiveData<String>().apply {
        addSource(dishList){
            val data = it.map { it.qty }
            this.value = "view cart ("+data.sum().toString()+" Items)"
        }
    }

    val totalCost: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(_cartList) {
            val list = emptyList<Int>().toMutableList()
            it.forEach {
                list.add(it.qty*it.costPerItem)
            }
            value = list.sum().toString() + euro
        }
    }

    fun deleteItem(position: Int) {
        val data = _dishList.value?.toMutableList()
        var count = data?.get(position)?.qty?.minus(1)!!
        if (count<=0){
            count = 0
        }
        data.get(position).qty = count
        _dishList.value = data
    }

    fun addItem(position: Int) {
        val data = _dishList.value?.toMutableList()
        var count = data?.get(position)?.qty?.plus(1)!!
        if (count>=20){
            count = 20
        }
        data.get(position).qty = count
        _dishList.value = data
    }

    fun deleteCartItem(position: Int) {
        val totalItem = _dishList.value
        val pos = totalItem?.indexOf(_cartList.value?.get(position))
        var count = pos?.let { totalItem.get(it).qty.minus(1) }!!
        if (count<=0){
            count = 0
        }
        totalItem.get(pos).qty = count
        _dishList.value = totalItem
    }

    fun addCartItem(position: Int) {
        val totalItem = _dishList.value
        val pos = totalItem?.indexOf(_cartList.value?.get(position))
        var count = pos?.let { totalItem.get(it).qty.plus(1) }!!
        if (count>=20){
            count = 20
        }
        totalItem.get(pos).qty = count
        _dishList.value = totalItem
    }

}
package com.app.doodleblue

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

interface AdapterClickListener{
    fun onItemClick(position: Int, value: CLICK)
}

enum class CLICK {
    DELETE,
    ADD
}

const val euro = "\u20ac"

class DishItemBinder(private val listener: AdapterClickListener) {

    private var data = MutableLiveData<DishModel>()
    private var position : Int? = null

    val title: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(data) {
            value = it.title
        }
    }
    val description: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(data) {
            value = it.description
        }
    }
    val qty: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(data) {
            value =
                if (it.qty!=0){
                    it.qty.toString()
                }else{
                    "Add"
                }
        }
    }
    val cost: LiveData<String> = MediatorLiveData<String>().apply {
        addSource(data) {
            value = it.costPerItem.toString() + euro
        }
    }

    val isAddVisible : LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(data){
            this.value = it.qty==0
        }
    }

    fun onSubClick(){
        position?.let { listener.onItemClick(it, CLICK.DELETE) }
    }
    fun onAddClick(){
        position?.let { listener.onItemClick(it, CLICK.ADD) }
    }
    fun onNewItemClick(){
        position?.let { listener.onItemClick(it, CLICK.ADD) }
    }


    fun bind(list: DishModel, postition: Int) {
        data.value = list
        this.position = postition
    }
}